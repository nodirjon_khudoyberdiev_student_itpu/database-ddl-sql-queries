-- Step 1: Create a Physical Database
CREATE DATABASE hospital_management_db;


-- Step 2: Create Schema
CREATE SCHEMA hospital;


-- Patients Table
CREATE TABLE hospital.patients (
    patient_id SERIAL PRIMARY KEY,
    first_name VARCHAR(50) NOT NULL,
    last_name VARCHAR(50) NOT NULL,
    gender CHAR(1) NOT NULL,
    date_of_birth DATE NOT NULL,
    address VARCHAR(255),
    phone_number VARCHAR(20),
    email VARCHAR(100)
);


-- Doctors Table
CREATE TABLE hospital.doctors (
    doctor_id SERIAL PRIMARY KEY,
    first_name VARCHAR(50) NOT NULL,
    last_name VARCHAR(50) NOT NULL,
    gender CHAR(1) NOT NULL,
    date_of_birth DATE NOT NULL,
    specialization VARCHAR(100),
    address VARCHAR(255),
    phone_number VARCHAR(20),
    email VARCHAR(100)
);


-- Medical Records Table
CREATE TABLE hospital.medical_records (
    record_id SERIAL PRIMARY KEY,
    patient_id INT NOT NULL,
    doctor_id INT NOT NULL,
    date DATE NOT NULL,
    diagnosis TEXT,
    treatment TEXT,
    CONSTRAINT fk_medical_records_patient_id FOREIGN KEY (patient_id) REFERENCES hospital.patients (patient_id),
    CONSTRAINT fk_medical_records_doctor_id FOREIGN KEY (doctor_id) REFERENCES hospital.doctors (doctor_id)
);


-- Appointments Table
CREATE TABLE hospital.appointments (
    appointment_id SERIAL PRIMARY KEY,
    patient_id INT NOT NULL,
    doctor_id INT NOT NULL,
    appointment_date DATE NOT NULL,
    appointment_time TIME NOT NULL,
    reason TEXT,
    CONSTRAINT fk_appointments_patient_id FOREIGN KEY (patient_id) REFERENCES hospital.patients (patient_id),
    CONSTRAINT fk_appointments_doctor_id FOREIGN KEY (doctor_id) REFERENCES hospital.doctors (doctor_id)
);


--Departments table
CREATE TABLE hospital.departments (
    department_id SERIAL PRIMARY KEY,
    department_name VARCHAR(100) NOT NULL,
    department_head INT, -- Assuming this is a reference to the doctor_id in the doctors table
    CONSTRAINT fk_department_head FOREIGN KEY (department_head) REFERENCES hospital.doctors (doctor_id)
);


--Prescriptin table
CREATE TABLE hospital.prescriptions (
    prescription_id SERIAL PRIMARY KEY,
    patient_id INT NOT NULL,
    doctor_id INT NOT NULL,
    prescription_date DATE NOT NULL,
    medication TEXT,
    dosage TEXT,
    CONSTRAINT fk_prescriptions_patient_id FOREIGN KEY (patient_id) REFERENCES hospital.patients (patient_id),
    CONSTRAINT fk_prescriptions_doctor_id FOREIGN KEY (doctor_id) REFERENCES hospital.doctors (doctor_id)
);




-- Ensure prescription dates are in the past or today
ALTER TABLE hospital.prescriptions
ADD CONSTRAINT chk_prescription_date_past_or_present
CHECK (prescription_date <= CURRENT_DATE);

-- Ensure visit dates in medical records are in the pas
ALTER TABLE hospital.medical_records
ADD CONSTRAINT date CHECK (date > '2000-01-01' AND date <= CURRENT_DATE),
ALTER COLUMN diagnosis SET NOT NULL,
ALTER COLUMN treatment SET NOT NULL;

-- Ensure appointment dates are in the future
ALTER TABLE hospital.appointments
ADD CONSTRAINT check_appointment_datetime 
CHECK (appointment_date >= current_date AND appointment_time BETWEEN '00:00:00' AND '23:59:59');

-- Patients Table
ALTER TABLE hospital.patients
ADD CONSTRAINT check_patient_gender 
CHECK (gender IN ('M', 'F'));

-- Doctors Table
ALTER TABLE hospital.doctors
ADD CONSTRAINT check_doctor_gender 
CHECK (gender IN ('M', 'F'));


ALTER TABLE hospital.departments
ADD CONSTRAINT department_name UNIQUE (department_name),
ALTER COLUMN department_head SET NOT NULL;

ALTER TABLE hospital.prescriptions
ADD CONSTRAINT prescription_id UNIQUE (prescription_id),
ALTER COLUMN medication SET NOT NULL,
ALTER COLUMN dosage SET NOT NULL;



-- Filling tables with data
INSERT INTO hospital.patients (first_name, last_name, gender, date_of_birth , address, phone_number , email)
VALUES ('John', 'Doe', 'M', '1990-05-15', '123 Main St', '123-456-7890', 'john.doe@example.com'),
       ('Jane', 'Smith', 'F', '1985-10-20', '456 Elm St', '987-654-3210', 'jane.smith@example.com'),
      ('Alice', 'Johnson', 'F', '1980-03-10', '789 Maple St', '111-222-3333', 'alice.johnson@example.com'),
    ('Robert', 'Williams', 'M', '1975-08-22', '456 Oak St', '444-555-6666', 'robert.williams@example.com'),
    ('Emily', 'Brown', 'F', '1992-05-05', '123 Pine St', '777-888-9999', 'emily.brown@example.com'),
    ('David', 'Martinez', 'M', '1988-11-15', '890 Cedar St', '222-333-4444', 'david.martinez@example.com'),
    ('Sophia', 'Garcia', 'F', '1970-09-28', '567 Birch St', '555-666-7777', 'sophia.garcia@example.com'),
    ('Michael', 'Taylor', 'M', '1983-06-17', '567 Cherry St', '777-888-9999', 'michael.taylor@example.com'),
    ('Sarah', 'Anderson', 'F', '1995-02-28', '890 Cedar St', '888-999-0000', 'sarah.anderson@example.com'),
    ('Christopher', 'Wilson', 'M', '1977-10-12', '123 Birch St', '111-222-3333', 'christopher.wilson@example.com'),
    ('Laura', 'Thomas', 'F', '1989-09-08', '456 Elm St', '222-333-4444', 'laura.thomas@example.com'),
    ('Matthew', 'Jackson', 'M', '1981-04-30', '789 Oak St', '333-444-5555', 'matthew.jackson@example.com');

INSERT INTO hospital.doctors (first_name, last_name, gender,date_of_birth, specialization , address, phone_number , email)
VALUES ('Michael', 'Johnson', 'M','1990-05-15', 'Cardiology', '789 Oak St', '555-123-4567', 'michael.johnson@example.com'),
       ('Emily', 'Davis', 'F','1990-05-15', 'Pediatrics', '321 Pine Ave', '555-987-6543', 'emily.davis@example.com'),
 ('Jessica', 'Lee', 'F', '1990-05-15','Neurology', '321 Elm St', '333-444-5555', 'jessica.lee@example.com'),
    ('Daniel', 'Rodriguez', 'M','1990-05-15', 'Orthopedics', '890 Walnut St', '666-777-8888', 'daniel.rodriguez@example.com'),
    ('Olivia', 'Lopez', 'F', '1990-05-15','Oncology', '456 Pine St', '999-000-1111', 'olivia.lopez@example.com'),
    ('Ethan', 'Gonzalez', 'M', '1990-05-15', 'Gastroenterology', '789 Cedar St', '888-999-0000', 'ethan.gonzalez@example.com'),
    ('Ava', 'Perez', 'F','1990-05-15', 'Dermatology', '234 Maple St', '123-456-7890', 'ava.perez@example.com'),
('Jennifer', 'Moore', 'F', '1990-05-15','Psychiatry', '678 Walnut St', '444-555-6666', 'jennifer.moore@example.com'),
    ('Kevin', 'White', 'M','1990-05-15', 'Endocrinology', '123 Maple St', '555-666-7777', 'kevin.white@example.com'),
    ('Maria', 'Hernandez', 'F','1990-05-15', 'Rheumatology', '456 Elm St', '666-777-8888', 'maria.hernandez@example.com'),
    ('Thomas', 'Young', 'M','1990-05-15', 'Urology', '789 Pine St', '777-888-9999', 'thomas.young@example.com'),
    ('Rachel', 'Scott', 'F','1990-05-15', 'Ophthalmology', '234 Oak St', '888-999-0000', 'rachel.scott@example.com');

-- Appointments Table
INSERT INTO hospital.appointments (patient_id, doctor_id, appointment_date, appointment_time, reason)
VALUES (1, 2, '2024-04-06', '09:00:00', 'Routine checkup'),
       (2, 2, '2024-04-07', '10:30:00', 'Follow-up appointment'),
 (3, 3, '2024-04-08', '11:00:00', 'Annual physical exam'),
    (4, 4, '2024-04-09', '14:00:00', 'Consultation for digestive issues'),
    (5, 5, '2024-04-10', '15:30:00', 'Follow-up for skin condition'),
(6, 6, '2024-04-11', '09:30:00', 'Initial consultation'),
    (7, 7, '2024-04-12', '10:00:00', 'Follow-up for thyroid condition'),
    (8, 8, '2024-04-13', '11:30:00', 'Annual checkup');

-- Prescriptions Table
INSERT INTO hospital.prescriptions (patient_id, doctor_id, prescription_date, medication, dosage)
VALUES (1, 2, '2024-04-06', 'Aspirin', '1 tablet Once daily for 7 days'),
       (2, 2, '2024-04-07', 'Amoxicillin', '250 mg Twice daily for 10 days'),
	(3, 3, '2024-04-07', 'Lisinopril', '10 mg Once daily for 30 days'),
    (4, 4, '2024-04-07', 'Omeprazole', '20 mg Twice daily for 14 days'),
    (5, 5, '2024-04-07', 'Topical corticosteroid Apply to affected area', 'As needed'),
   (6, 6, '2024-04-07', 'Sertraline', '50 mg Once daily for 30 days'),
    (7, 7, '2024-04-07', 'Levothyroxine', '100 mcg Once daily for 90 days'),
    (8, 8, '2024-04-07', 'Multivitamin', '1 tablet Once daily for 30 days');

-- Medical Records Table
INSERT INTO hospital.medical_records (patient_id, doctor_id, date, diagnosis, treatment)
VALUES (1, 2, '2024-04-06', 'Hypertension', 'Prescribed medication and lifestyle modifications'),
       (2, 2, '2024-04-07', 'Upper respiratory infection', 'Prescribed antibiotics and rest'),
(3, 3, '2024-04-08', 'General checkup', 'Advised on maintaining a healthy lifestyle'),
    (4, 4, '2024-04-09', 'Gastritis', 'Prescribed medication and dietary recommendations'),
    (5, 5, '2024-04-10', 'Eczema', 'Prescribed topical medication and provided skincare tips'),
(6, 6, '2024-04-11', 'Depression', 'Prescribed medication and therapy sessions'),
    (7, 7, '2024-04-12', 'Hypothyroidism', 'Prescribed thyroid hormone replacement therapy'),
    (8, 8, '2024-04-13', 'General checkup', 'Provided preventive care advice');

-- Departments Table
INSERT INTO hospital.departments (department_name, department_head)
VALUES ('Neurology', 2),
    ('Orthopedics', 2),
    ('Oncology', 3),
    ('Gastroenterology', 4),
    ('Dermatology', 5),
 ('Psychiatry', 6),
    ('Endocrinology', 7),
    ('Rheumatology', 8),
    ('Urology', 9),
    ('Ophthalmology', 10);


Patients Table
ALTER TABLE hospital.patients 
ADD COLUMN record_ts DATE DEFAULT current_date;

-- Check if 'record_ts' has been set for existing rows
SELECT COUNT(*) FROM hospital.patients WHERE record_ts IS NULL;

-- Doctors Table
ALTER TABLE hospital.doctors 
ADD COLUMN record_ts DATE DEFAULT current_date;

-- Check if 'record_ts' has been set for existing rows
SELECT COUNT(*) FROM hospital.doctors WHERE record_ts IS NULL;

-- Appointments Table
ALTER TABLE hospital.appointments 
ADD COLUMN record_ts DATE DEFAULT current_date;

-- Check if 'record_ts' has been set for existing rows
SELECT COUNT(*) FROM hospital.appointments WHERE record_ts IS NULL;

-- Prescriptions Table
ALTER TABLE hospital.prescriptions 
ADD COLUMN record_ts DATE DEFAULT current_date;

-- Check if 'record_ts' has been set for existing rows
SELECT COUNT(*) FROM hospital.prescriptions WHERE record_ts IS NULL;

-- Medical Records Table
ALTER TABLE hospital.medical_records 
ADD COLUMN record_ts DATE DEFAULT current_date;

-- Check if 'record_ts' has been set for existing rows
SELECT COUNT(*) FROM hospital.medical_records WHERE record_ts IS NULL;

-- Departments Table
ALTER TABLE hospital.departments 
ADD COLUMN record_ts DATE DEFAULT current_date;

-- Check if 'record_ts' has been set for existing rows
SELECT COUNT(*) FROM hospital.departments WHERE record_ts IS NULL;
